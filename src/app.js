const overlayBackdrop = document.querySelector('.overlay-backdrop')

const disableOverlayBackdrop = (callback) => {
    if (callback) callback()
    overlayBackdrop.removeEventListener('click', disableOverlayBackdrop)
    overlayBackdrop.style.display = 'none'
}

const enableOverlayBackdrop = (callback) => {
    overlayBackdrop.addEventListener('click', () => disableOverlayBackdrop(callback))
    overlayBackdrop.style.display = 'block'
}

// Initializes profile script
(profileWrapper => {
    if (!profileWrapper) return;
    const editProfileElement    = document.querySelector('.edit-profile-button')
    const cancelActionElement   = document.querySelector('.editing-action__cancel-btn')

    const enableEditing  = () => profileWrapper.classList.add('editing')
    const disableEditing = () => profileWrapper.classList.remove('editing');

    // Events
    editProfileElement.addEventListener('click', enableEditing)
    cancelActionElement.addEventListener('click', disableEditing)
    
})(document.querySelector('.profile-wrapper'));

// Initializes edit profile items

(editableItems => {
    if (editableItems.length === 0) return
    const _editableItems = []
    
    for(let i = 0; i < editableItems.length; i++)
        _editableItems.push(editableItems[i])

    const popItemEdit = (ev) => {
        ev.stopPropagation()
        const { currentTarget } = ev
        const popEdit = document.querySelector('.profile-wrapper__pop-edit')
        const selectedItem = document.getElementById(currentTarget.getAttribute('data-edit'))

        // Set pop edit postion according to target position
        popEdit.style.top = `${currentTarget.offsetTop - 10}px`
        popEdit.style.left = `${currentTarget.offsetLeft + 10}px`
        
        // Copy edit text-field to pop-edit
        popEdit.querySelector('.text-field').innerHTML = selectedItem.innerHTML

        enableOverlayBackdrop(() => popEdit.style.display = 'none')

        popEdit.style.display = 'block'
    }
    
    _editableItems.forEach(element => element.addEventListener('click', popItemEdit))

})(document.getElementsByClassName('list__item__edit'));
