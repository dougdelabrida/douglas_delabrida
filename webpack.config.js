const path = require('path')

module.exports = {
    entry: path.resolve(__dirname, 'src/app.js'),
    output: {
        path: path.resolve(__dirname, 'public/js/'),
        filename: 'app.min.js'
    },
    module : {
        loaders : [
            {
                test : /\.js?/,
                include : path.resolve(__dirname, './src'),
                exclude : /node_modules/,
                loader : 'babel-loader'
            }
        ]
    }
}
