const gulp = require('gulp')
const webpack = require('webpack')
const webpackStream = require('webpack-stream')
const connect = require('gulp-connect')

const sass = require('gulp-sass')
const autoprefix = require('gulp-autoprefixer')
const rename = require('gulp-rename')
const sourcemaps = require('gulp-sourcemaps')
const gutil = require('gulp-util')
const livereload = require('gulp-livereload')

const webpackConfig = require('./webpack.config.js')

webpackConfig.watch = true
webpackConfig.devtool = 'cheap-module-eval-source-map'
webpackConfig.plugins = [
  new webpack.DefinePlugin({
    'process.env.NODE_ENV': '"development"'
  })
]

gulp.task('default', ['connect', 'watch', 'webpack', 'scss'])

gulp.task('webpack', () => (
  gulp.src('./src/app.js')
    .pipe(webpackStream(webpackConfig))
    .on('error', function(error) {
      gutil.log(error.message)
      this.emit('end')
    })
    .pipe(gulp.dest('./public/js/'))
    .pipe(livereload())
))

gulp.task('scss', () => (
  gulp.src('./src/scss/main.scss')
    .pipe(sourcemaps.init())
    .pipe(sass({
        style: 'compressed'
      }).on('error', function(error) {
        gutil.log(error.message)
        this.emit('end')
      })
    )
    .pipe(autoprefix('last 2 versions'))
    .pipe(rename('app.css'))
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest('./public/css'))
    .pipe(livereload())
))

gulp.task('watch', () => {
  livereload.listen()
  gulp.watch(['./src/scss/**/*.scss'], ['scss'])
  gulp.watch(['./public/css/app.css'], function(file) {
    livereload.changed(file)
  })
  gulp.watch(['./public/*.html'], ['html'])
})

gulp.task('connect', function() {
  connect.server({
    root: 'public',
    livereload: true
  })
})

gulp.task('html', () => {
  gulp.src('./public/*.html')
    .pipe(livereload())
})
