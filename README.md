# Social Front

## Installation guide

Access the demo here [http://douglas-delabrida-front.surge.sh/](http://douglas-delabrida-front.surge.sh/)

Requirements 

* [NodeJS](https://nodejs.org/en/download/) V6 (or higher) - For development
* [Git](https://git-scm.com/downloads) - For version control

Open your terminal and follow theses steps

1 - Clone this repo into a folder:

`git clone https://dougdelabrida@bitbucket.org/dougdelabrida/douglas_delabrida.git`
after cloning, navigate to the new folder `cd douglas_delabrida`

2 - Install the dependencies:

`npm install`

`npm install -g gulp` In case you don't have gulp installed globally


## Development
Once having followed the Installation Guide, you're able to develop

In the project's folder run `gulp` in your terminal

Now the project is available at `http://localhost:8080`

